<?php

/**
 * @file
 * uw_ct_bibliography.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_bibliography_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = TRUE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'publications_related_to_people';
  $context->description = 'Biblio publications related to person profiles';
  $context->tag = 'Content';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'uw_ct_person_profile' => 'uw_ct_person_profile',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-uw_publications-block_2' => array(
          'module' => 'views',
          'delta' => 'uw_publications-block_2',
          'region' => 'content',
          'weight' => '47',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Biblio publications related to person profiles');
  t('Content');
  $export['publications_related_to_people'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'publications_search_sidebar';
  $context->description = 'Display search block on the right sidebar.';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'publications' => 'publications',
        'publications-search' => 'publications-search',
        'publications-search?*' => 'publications-search?*',
        'publications/author-asc' => 'publications/author-asc',
        'publications/author-desc' => 'publications/author-desc',
        'publications/type-asc' => 'publications/type-asc',
        'publications/type-desc' => 'publications/type-desc',
        'publications/year-asc' => 'publications/year-asc',
        'publications/year-desc' => 'publications/year-desc',
        'publications/author/*' => 'publications/author/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-33086f44c966565ff835629aa795c712' => array(
          'module' => 'views',
          'delta' => '33086f44c966565ff835629aa795c712',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display search block on the right sidebar.');
  $export['publications_search_sidebar'] = $context;

  return $export;
}
