<?php

/**
 * @file
 * uw_ct_bibliography.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_bibliography_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-manager-vocabularies_bibliography-keywords:admin/structure/taxonomy/bibliography_keywords.
  $menu_links['menu-site-manager-vocabularies_bibliography-keywords:admin/structure/taxonomy/bibliography_keywords'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/bibliography_keywords',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Bibliography keywords',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_bibliography-keywords:admin/structure/taxonomy/bibliography_keywords',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Bibliography keywords');

  return $menu_links;
}
